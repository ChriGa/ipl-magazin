<?php
/**
 * @package     Joomla.Site / tab-mod by cg@089webdesign.de / web-loves-you.com
 * @subpackage  mod_articles_catShowcase
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>


<div id="myCarousel" class="carousel slide">
	<ul class="nav nav-tabs category-module<?php echo $moduleclass_sfx; ?>" id="myTab">
		<?php foreach ($list as $item) : ?>
			<li class="navTabsLi">
				<?php 
					$rawCattitle =  $item->category_title;
					$catTitle = strtolower(str_replace("-","", $rawCattitle));
				?>
				<?php print '<a href="#'.$catTitle.'" data-toggle="tab">'.$catTitle.'</a><br /><p>'.$item->title.'</p>'; ?>
			</li>		
	<?php endforeach; ?>
	</ul>	
	<div class="tab-content carousel-inner">
		<?php foreach ($list as $item) : 
		$catTitle = $item->category_title;
		$images = json_decode($item->images);
		$intrImage = $images->image_intro; 
		//preprint($intrImage); ?>
			<div class="tab-pane item" id="<?=$catTitle?>">
				<?php if ($params->get('show_introtext') && !empty($intrImage)) : ?>
					<div class="row-fluid">
						<?//preprint($list); ?>
						<div class="span6 mod-articles-category-introtext">
							<?php echo $item->introtext; ?>
						</div>
						<div class="span6 mod-articles-introImg">
							<img src="<?=$intrImage?>" alt="<?=$item->title ?>-Beitragsbild"/>
						</div>
					</div>
					<?php else : ?>
					<p class="mod-articles-category-introtext">
						<?php echo $item->introtext; ?>
					</p>
				<?php endif; ?>
				<?php if ($params->get('show_readmore')) : ?>
					<p class="mod-articles-category-readmore">
						<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
							<?php if ($item->params->get('access-view') == false) : ?>
								<?php echo JText::_('MOD_ARTICLES_CATEGORY_REGISTER_TO_READ_MORE'); ?>
							<?php elseif ($readmore = $item->alternative_readmore) : ?>
								<?php echo $readmore; ?>
								<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
							<?php elseif ($params->get('show_readmore_title', 0) == 0) : ?>
								<?php echo JText::sprintf('MOD_ARTICLES_CATEGORY_READ_MORE_TITLE'); ?>
							<?php else : ?>
								<?php echo JText::_('MOD_ARTICLES_CATEGORY_READ_MORE'); ?>
								<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
							<?php endif; ?>
						</a>
					</p>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	</div>
</div>
<script type="text/javascript">
	/*BS Tab*/
		jQuery('#myTab a').click(function (e) {
		  e.preventDefault();
		  jQuery(this).tab('show');
		});
			jQuery('#myTab a:first').tab('show'); // Select first tab
	/*BS Carousel*/
	jQuery('.carousel').carousel({
		  interval: 3000
		});
/*jQuery(document).ready(function() {
	jQuery("navTabsLi").addClass("active").delay(3000).dequeue(function(next){
    	jQuery(this).removeClass("active");
    	next();
	});
});*/

/*jQuery(document).ready(function() {
	jQuery('.navTabsLi').each(function(i){
	  var row = jQuery(this);
	  setTimeout(function() {
	    row.toggleClass('active');
	  }, 1000*i);
	})
})*/

</script>
