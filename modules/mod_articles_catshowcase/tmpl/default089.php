<?php
/**
 * @package     Joomla.Site / tab-mod by cg@089webdesign.de / web-loves-you.com / original Script siehe: http://lopatin.github.io/sliderTabs/
 * @subpackage  mod_articles_catShowcase
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::script('modules/mod_articles_catshowcase/js/jquery.sliderTabs.js');
JHtml::_('stylesheet', 'modules/mod_articles_catshowcase/css/jquery.sliderTabs.min.css');

		require_once 'templates/089-iplmagazin/Mobile_Detect.php';
		$detect = new Mobile_Detect;

?>
<?php if (!$detect->isMobile() || $detect->isTablet() ) : ?>
	<div id="sliderTabs">
		<ul class="category-module<?php echo $moduleclass_sfx; ?>" >
			<?php foreach ($list as $item) : 
					$rawCattitle =  $item->category_title;
					$catTitle = strtolower(str_replace(" ","", $rawCattitle));	?>
						<?php print '<li><a href="#'.$catTitle.'"><span>'.$item->category_title.'</span><br />'.$item->title.'</a></li>'; ?>	
		<?php endforeach; ?>
		</ul>	
			<?php foreach ($list as $item) : 
				$catTitle = $item->category_title;
				$images = json_decode($item->images);
				$intrImage = $images->image_intro; 
				$rawCattitle =  $item->category_title;
				$catTitle = strtolower(str_replace(" ","", $rawCattitle)); ?>
			<div id="<?=$catTitle?>">
				<?php if ($params->get('show_introtext') && !empty($intrImage)) : ?>
					<h3 class="tabSliderH3"><?php print $item->title; ?></h3>
						<div class="row-fluid">
							<div class="span6 mod-articles-category-introtext">
								<?php echo $item->introtext; ?>
								<?php if ($params->get('show_readmore')) : ?>
									<p class="mod-articles-category-readmore">
										<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
											weiterlesen ...
										</a>
									</p>
								<?php endif; ?>
							</div>
							<div class="span6 mod-articles-introImg">
								<img src="<?=$intrImage?>" alt="<?=$item->title ?>-Beitragsbild"/>
								<?php if($images->image_intro_caption) : ?>
									<div class="iplImgCaption">
										<p><?php print $images->image_intro_caption; ?></p>
									</div>
								<?php endif; ?>								
							</div>
						</div>
					<?php else : ?>
						<p class="mod-articles-category-introtext">
							<?php echo $item->introtext; ?>
						</p>
						<?php if ($params->get('show_readmore')) : ?>
							<p class="mod-articles-category-readmore">
								<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
									weiterlesen ...
								</a>
							</p>
						<?php endif; ?>					
				<?php endif; ?>
				<? /* 
					* CG dynamisches auslesen der weiterlesen-link Parameter - ist hier nicht nötig aber lieber aufheben
					*
				<?php if ($params->get('show_readmore')) : ?>
					<p class="mod-articles-category-readmore">
						<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
							weiterlesen ...
							<?php if ($item->params->get('access-view') == false) : ?>
								<?php echo JText::_('MOD_ARTICLES_CATEGORY_REGISTER_TO_READ_MORE'); ?>
							<?php elseif ($readmore = $item->alternative_readmore) : ?>
								<?php echo $readmore; ?>
								<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
							<?php elseif ($params->get('show_readmore_title', 0) == 0) : ?>
								<?php echo JText::sprintf('MOD_ARTICLES_CATEGORY_READ_MORE_TITLE'); ?>
							<?php else : ?>
								<?php echo JText::_('MOD_ARTICLES_CATEGORY_READ_MORE'); ?>
								<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
							<?php endif; ?>
						</a>
					</p>
				<?php endif; ?> */ ?>
			</div>
		<?php endforeach; ?>
	</div>
	<script type="text/javascript">
	jQuery.noConflict();
		var slider = jQuery("div#sliderTabs");
			jQuery(document).ready(function() {
				slider.sliderTabs({
				  autoplay: 6000,
				  mousewheel: false,
				  transitionSpeed: 1500,
				  tabs: true,
				  position: "top",
				  transition: "fade",
				  panelArrows: false,
				  tabArrowWidth: 0,
				  pauseOnHover: true
				});
			});
	</script>

<?php else: ?>
 
	<?php foreach ($list as $item) : 
		$catTitle = $item->category_title;
	?>
	<?php //print_r($item); ?>
		<div class=" iplH3 iplMag-shadow">
			<h3><?php print $catTitle; ?></h3>
			<div class="mobileCatAlt ">
				<h4><?php print $item->title; ?></h4>
				<div class="intrTextNews">
					<?php echo $item->introtext; ?>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
		
<?php endif; ?>
