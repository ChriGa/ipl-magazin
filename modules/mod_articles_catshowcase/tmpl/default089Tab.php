<?php
/**
 * @package     Joomla.Site / tab-mod by cg@089webdesign.de / web-loves-you.com
 * @subpackage  mod_articles_catShowcase
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>



	<ul class="nav nav-tabs category-module<?php echo $moduleclass_sfx; ?>" >
		<?php foreach ($list as $item) : ?>
			<li class="navTabsLi">
				<?php 
					$rawCattitle =  $item->category_title;
					$catTitle = strtolower(str_replace("-","", $rawCattitle));
				?>
				<?php print '<a href="#'.$catTitle.'" data-toggle="tab">'.$catTitle.'</a><br /><p>'.$item->title.'</p>'; ?>
			</li>		
	<?php endforeach; ?>
	</ul>	
	<div class="tab-content">		
		<?php
			foreach ($list as $item) : 
				$catTitle = $item->category_title;
				$images = json_decode($item->images);
				$intrImage = $images->image_intro;
				$rawCattitle =  $item->category_title;
				$catTitle = strtolower(str_replace("-","", $rawCattitle));
			//if($item->catid == 58) print_r( $item->title." is der Titel zur Kategorie ". $item->catid); 
			//preprint($list); ?>

			<div id="<?=$catTitle?>" class="tab-pane fade in ">

				<?php if ($params->get('show_introtext') && !empty($intrImage)) : ?>
					<div class="row-fluid">
						<?//preprint($list); ?>
						<div class="span6 mod-articles-category-introtext">
							<?php echo $item->introtext; ?>
						</div>
						<div class="span6 mod-articles-introImg">
							<img src="<?=$intrImage?>" alt="<?=$item->title ?>-Beitragsbild"/>
						</div>
					</div>
					<?php else : ?>
					<p class="mod-articles-category-introtext">
						<?php echo $item->introtext; ?>
					</p>
				<?php endif; ?>
				<?php if ($params->get('show_readmore')) : ?>
					<p class="mod-articles-category-readmore">
						<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
							<?php if ($item->params->get('access-view') == false) : ?>
								<?php echo JText::_('MOD_ARTICLES_CATEGORY_REGISTER_TO_READ_MORE'); ?>
							<?php elseif ($readmore = $item->alternative_readmore) : ?>
								<?php echo $readmore; ?>
								<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
							<?php elseif ($params->get('show_readmore_title', 0) == 0) : ?>
								<?php echo JText::sprintf('MOD_ARTICLES_CATEGORY_READ_MORE_TITLE'); ?>
							<?php else : ?>
								<?php echo JText::_('MOD_ARTICLES_CATEGORY_READ_MORE'); ?>
								<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
							<?php endif; ?>
						</a>
					</p>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	</div>

<script type="text/javascript">
jQuery.noConflict();
/*BS Tab*/
/*jQuery('.nav-tabs a').click(function (e) {
	  e.preventDefault();
	  jQuery(this).tab('show');	
	});*/

	jQuery('.nav-tabs a:first').tab('show'); // Select first tab

jQuery(function(){
    loopIt();
});

function loopIt(){    
/*    jQuery('.tab-pane' ).each(function(i){
      var t=jQuery(this);
      setTimeout(function(){
        t.addClass('active');
          setTimeout(function(){
            t.removeClass('active');
              if(i==jQuery('.tab-pane').length-1) loopIt();;
          }, 2000);
      }, 2100*i);
    });*/

// !!
/*    jQuery('.nav-tabs a').each(function(i){
      var t=jQuery(this);
      setTimeout(function(){
        t.tab('show')
          setTimeout(function(){
        	t.tab('hide')
              if(i==t.tab('show').length-1) loopIt();;
          }, 2000);
      }, 2100*i);
    }); */
}

/*	jQuery(document).ready(function() {
		jQuery('.tab-pane').first().addClass('active');
	})*/
</script>