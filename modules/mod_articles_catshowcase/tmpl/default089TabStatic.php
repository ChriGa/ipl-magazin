<?php
/**
 * @package     Joomla.Site / tab-mod by cg@089webdesign.de / web-loves-you.com
 * @subpackage  mod_articles_catShowcase
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>

<?/*

	<ul class="nav nav-tabs category-module<?php echo $moduleclass_sfx; ?>" id="myTab">
		<?php foreach ($list as $item) : ?>
			<li class="">
				<?php 
					$rawCattitle =  $item->category_title;
					$catTitle = strtolower(str_replace("-","", $rawCattitle));
				?>
				<?php print '<a href="#'.$catTitle.'" data-toggle="tab">'.$catTitle.'</a><br /><p>'.$item->title.'</p>'; ?>
			</li>		
	<?php endforeach; ?>
	</ul>	
	<div class="tab-content">		
		<?php
			foreach ($list as $item) : 
				$catTitle = $item->category_title;
				$images = json_decode($item->images);
				$intrImage = $images->image_intro;
			//preprint($intrImage); ?>
			<div class="tab-pane fade" id="<?=$catTitle?>">
				<?php if ($params->get('show_introtext') && !empty($intrImage)) : ?>
					<div class="row-fluid">
						<?//preprint($list); ?>
						<div class="span6 mod-articles-category-introtext">
							<?php echo $item->introtext; ?>
						</div>
						<div class="span6 mod-articles-introImg">
							<img src="<?=$intrImage?>" alt="<?=$item->title ?>-Beitragsbild"/>
						</div>
					</div>
					<?php else : ?>
					<p class="mod-articles-category-introtext">
						<?php echo $item->introtext; ?>
					</p>
				<?php endif; ?>
				<?php if ($params->get('show_readmore')) : ?>
					<p class="mod-articles-category-readmore">
						<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
							<?php if ($item->params->get('access-view') == false) : ?>
								<?php echo JText::_('MOD_ARTICLES_CATEGORY_REGISTER_TO_READ_MORE'); ?>
							<?php elseif ($readmore = $item->alternative_readmore) : ?>
								<?php echo $readmore; ?>
								<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
							<?php elseif ($params->get('show_readmore_title', 0) == 0) : ?>
								<?php echo JText::sprintf('MOD_ARTICLES_CATEGORY_READ_MORE_TITLE'); ?>
							<?php else : ?>
								<?php echo JText::_('MOD_ARTICLES_CATEGORY_READ_MORE'); ?>
								<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
							<?php endif; ?>
						</a>
					</p>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	</div> */ ?>

  <ul class="nav nav-tabs">
    <li class="active"><a href="#home">Home</a></li>
    <li><a href="#menu1">Menu 1</a></li>
    <li><a href="#menu2">Menu 2</a></li>
    <li><a href="#menu3">Menu 3</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h3>HOME</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    </div>
    <div id="menu1" class="tab-pane fade">
      <h3>Menu 1</h3>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
    <div id="menu2" class="tab-pane fade">
      <h3>Menu 2</h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
    </div>
    <div id="menu3" class="tab-pane fade">
      <h3>Menu 3</h3>
      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>
  </div>


<script>
$(document).ready(function(){
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
});
</script>


<script type="text/javascript">

jQuery(document).ready(function(){
    jQuery(".nav-tabs a").click(function(){
        jQuery(this).tab('show');
    });
});


/*		jQuery('#myTab a').click(function (e) {
		  e.preventDefault();
		  jQuery(this).tab('show');	
		});*/
			jQuery('#myTab a:first').tab('show'); // Select first tab





/*setInterval(autoToggle, 1000);
function autoToggle() {
    element.find('>:tab-content').toggleClass('active').next('.tab-pane');
}*/

/*
jQuery(function(){
    loopIt();
});

function loopIt(){    
    jQuery('.tab-pane' ).each(function(i){
      var t=jQuery(this);
      setTimeout(function(){
        t.addClass('active');
          setTimeout(function(){
            t.removeClass('active');
              if(i==jQuery('.tab-pane').length-1) loopIt();;
          }, 2000);
      }, 2100*i);
    });

    jQuery('.navTabsLi' ).each(function(i){
      var t=jQuery(this);
      setTimeout(function(){
        t.addClass('active');
          setTimeout(function(){
            t.removeClass('active');
              if(i==jQuery('.navTabsLi').length-1) loopIt();;
          }, 2000);
      }, 2100*i);
    });
}*/
</script>
<script type="text/javascript">
/*	jQuery(document).ready(function() {
		jQuery('.tab-pane').first().addClass('active');
	})*/
	/*BS Tab*/

</script>