<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>

<header class="header">
	<div class="header-wrapper">
      <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
      <div class="container ">
		<div class="row-fluid">           
			<div class="header-inner clearfix">
				<div class="span5 logo">
					<a class="brand" href="<?php echo JURI::root(); ?>">							
						<div class="logo-wrapper"><?php print $logo; ?></div>
					</a>
				</div>
				<div class="span4 ausgabe-wrapper">
					<a href="/archiv-downloads" title="Die neueste Ausgabe des IPL Magazins ansehen">
						<div class="ausgabe">Ausgabe <?=$ausgabeNo ?> / <?=$ausgabeMonat . ' ' . date('y');?></div>				
					</a>
				</div>
			</div>      
		</div>
			<?php if ($this->params->get('sitedescription')) : ?>
				<?php echo '<div class="span8 site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
			<?php endif; ?>
		<div class="span4 header-right">
			<jdoc:include type="modules" name="head-right" style="custom" />
		</div>
      </div> <!-- /.container -->
    </div>
</header>