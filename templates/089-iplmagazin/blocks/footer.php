<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div id="footerBanner"></div>
<footer class="clear-footer" role="contentinfo">				
	<div class="row-fluid footer">
		<div class="span4">
			<img class="footerLogo" src="/images/content/logo/ipl-magazin-logo.png" alt="IPL Magazin Logo" />
		</div>	
		<div class="span8">
			<h2 class="footerH2">Institut für Produktionsmanagement und Logistik</h2>
		</div>		
	</div>		
	<div class="copyright">
		<p>&copy; 2007 - <?php print date("Y") ?> IPL Magazin. Alle Rechte vorbehalten.</p>
	</div>
</footer>
<div class="row-fluid">
		<div class="footer-bottom">
			<div class="span4">
				<a href="http://www.i-p-l.de/" target="_blank" alt="IPL GmbH">
					<img src="images/ipl-slogans.svg" alt="IPL, Forschungen, Schulungen, Zertifizierungen, Strategien" />
				</a>
			</div>
			<div class="span4">
				<a href="http://ipl-beratung.de" target="_blank" alt="IPL-Beratung">
					<img src="images/ipl-beratung.svg" alt="IPL, Beratung, Analysen, Fabrikplanung, Prozesse, Realisierung" />
				</a>
			</div>
			<div class="span4">
				<a href="http://www.visual-technologies.de/" target="_blank" alt="Visual Technologies">
					<img src="images/ipl-visual-technologies.svg" alt="IPL, Visual Technologies, Software, Virtual Augmented Reality" />
				</a>
			</div>			
		</div>
	<div class="clr"></div>
</div>
	
		