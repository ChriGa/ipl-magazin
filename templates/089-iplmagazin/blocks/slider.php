<?php
/**
 * @author   	cg@089webdesgin.de
 * @copyright   Copyright (C) 2016 089webdesgin.de. All rights reserved.
 * @URL 		http://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;

/*ist aktuelle Seite die Startseite?*/
$app = JFactory::getApplication();
$menu = $app->getMenu();
if ($menu->getActive() == $menu->getDefault())  : ?>
	<?php if (!$detect->isMobile() || $detect->isTablet()) : ?>
		<div class="row-fluid">
			<div id="bannerWrapper">
				<div class="mbHeader">
					<h1><?=$mbHeadline?></h1>
				</div>
				<div id="magBanner" class="span12">
					<div class="mbImg span4">
						<a href="/">
							<img id="mbImage" src="<?=$mbImage?>" alt="Cover des IPL-Magazins Ausgabe <?=$ausgabeNo.' '.$ausgabeMonat.' '.date('y'); ?>" 
								title="Cover des IPL-Magazins Ausgabe <?=$ausgabeNo.' '.$ausgabeMonat.' '.date('y'); ?>"/>
						</a>
					</div>
					<div class="dlBanner span3">
						<div class="dlBanner-wrapper iplMag-shadow">
							<h3 class="dlBannerHeader">PDF-Direkt-Download</h3>
							<a class="dlBannerLink" href="/<?php print $mbLink ?>" target="_blank">
								Aktuelle Ausgabe <?=$ausgabeMonat.' '.date('Y')?>
							</a>
						</div>
					</div>
					<div class="vorwort-Banner span5">
						<div class="vorwort-inner">
							<jdoc:include type="modules" name="vorwort" style="custom" />
						</div>
					</div>
				</div>
				<div id="mbFrame"></div>
				<div id="border-bottom" class="iplMag-shadow"></div>				
			</div>
		</div>
		<style type="text/css">
			
			div#bannerWrapper {
				position: relative;
				height: 420px;
			}

			#magBanner {
				position: relative;
				z-index: 3;
				height: inherit;
			}
			
			img#mbImage {
				transform: rotate(<?=$imgDegree?>deg);
				position: relative;
				top: <?=$imgTop / 2 ?>px;
				z-index: 0;
				box-shadow: 5px 7px 14px 3px #D4D4D4;
				max-width: 100%;
			}
		@media(max-width: 1200px) {
			body.site div.mbHeader h1 {
				font-size: calc(33px + 24 * (100vw - 1200px) / 1200);
			}
			body.site #magBanner {	
				max-height: 420px;
			}
		}
		@media(min-width: 1200px) {
			img#mbImage {
				top: <?=$imgTop?>px;
			}
		}

			.mbHeader {
			    position: absolute;
			    right: 10px;
			    top: 60px;
			}
			div.mbHeader h1 {
				text-transform: uppercase;
				font-size: 33px;
				color: #68696a;
			}
			div.dlBanner {
				position: relative;
				height: inherit;
				right: 40px;
		    	z-index: -1;
			}

			h3.dlBannerHeader,
			a.dlBannerLink {
			    font-size: 16px;
			    color: #68696a;
			    padding: 5px 20px;
			}
			h3.dlBannerHeader {
			    background: #e6effb;
			}
			a.dlBannerLink {
				text-decoration: underline;
			}	
			.dlBanner-wrapper {
			    position: absolute;
			    bottom: 150px;
				border: 1px solid #e3e2e2;
			    padding: 5px 0px 20px 0px;
			}
			div#mbFrame {
			    height: 286px;
			    position: absolute;
			    bottom: 0;
			    width: 100%;
			    z-index: 2;
			    background: transparent;
			    border: 2px solid #e2e2e3;
		    	border-top: 20px solid #f5f6f6;
			}	
			div#border-bottom {
			    position: absolute;
			    bottom: 0px;
			    height: 32px;
			    width: 100%;
			    margin-bottom: 0px;
			    z-index: 5;
			    background: transparent;
			    box-shadow: 0px 12px 10px -10px #B1B1B1;
			}
			.vorwort-Banner {
			    position: relative;
			    height: inherit;
			}
			.vorwort-inner {
			    position: absolute;
			    bottom: 37%;
		   		transform: translateY(50%);
			}

			.vorwort-inner h4 {
				color: #4e4f4f;
			}
			div.mbHeader h1,
			.vorwort-inner h3,
			.vorwort-inner h4,
			.vorwort-banner a.readmore {
				text-shadow: 0px 4px 10px #cacaca;
			}
		</style>
	<?php else : ?>
	<div class="clr"></div>
			<div class="mbHeader">
				<p class="announce hidden-desktop">Thema der aktuellen Ausgabe: </p>
				<h1><?=$mbHeadline?></h1>
			</div>
			<div id="bannerWrapperMobile">
				<div class="mbImgMobile ">
					<a href="/">
						<img id="mbImageMobile" src="<?=$mbImage?>" alt="Cover des IPL-Magazins Ausgabe <?=$ausgabeNo.' '.$ausgabeMonat.' '.date('y'); ?>" 
							title="Cover des IPL-Magazins Ausgabe <?=$ausgabeNo.' '.$ausgabeMonat.' '.date('y'); ?>"/>
					</a>
				</div>
				<div class="dlBannerMobile ">
					<div class="dlBanner-wrapper">
						<h3 class="dlBannerHeader">PDF-Direkt-Download</h3>
						<a class="dlBannerLink" href="/<?php print $mbLink; ?>" target="_blank">
							Aktuelle Ausgabe <?=$ausgabeMonat.' '.date('Y')?>
						</a>
					</div>
				</div>
			</div>
			<div class="vorwort-BannerMobile ">
				<div class="vorwort-inner">
					<jdoc:include type="modules" name="vorwort" style="custom" />
				</div>
			</div>			
	<?php endif; ?>
<?php endif; ?>