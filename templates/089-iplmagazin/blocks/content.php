<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$span = 12;
if ($this->countModules('left') && $this->countModules('right')) $span = 6;
if (!$this->countModules('left') && !$this->countModules('right') && !$this->countModules('desktop-right')) $span = 12;
if (!$detect->isMobile() || $this->countModules('dektop-right')) $span = 9;
if (!$this->countModules('left') && !$this->countModules('right') && !$this->countModules('desktop-right')) $span = 12;

?>
<div class="clear-container">
	<div class="content">
		<jdoc:include type="modules" name="banner" style="xhtml" />
		<div class="row-fluid">
		
			<?php if ($this->countModules('left')) : ?>
				<div class="span3 clear-left">
					<jdoc:include type="modules" name="left" style="xhtml" />
				</div>				
			<?php endif; ?>
			
			<main id="content" role="main" class="span<?php print ($frontPage || $menu->getActive() == 111) ? "span12" :  $span; ?>"><?php // override wg Archiv und Downloads ACHTUNG span12?>
			<!-- Begin Content -->
				<jdoc:include type="message" />
				<jdoc:include type="modules" name="content-center" style="custom" />
			<?php //Falls Startseite component NICHT ausgeben -CG
			//if(!$menu->getActive() == $menu->getDefault()) : ?>
				<jdoc:include type="component" />
			<?php// endif;?>
			<?php if($this->countModules('content-bottom')) :?>
				<jdoc:include type="modules" name="content-bottom" style="custom" />
			<?php endif;?>
			<!-- End Content -->
			</main>
			
			<?php if ($this->countModules('right')) : ?>
				<div class="span3 clear-right">		
					<jdoc:include type="modules" name="right" style="xhtml" />
				</div>
			<?php endif; ?>
			<?php if ($this->countModules('desktop-right') && !$detect->isMobile() ) : ?>
				<div class="span3">		
					<jdoc:include type="modules" name="desktop-right" style="xhtml" />
				</div>
			<?php endif; ?>
			
		</div>
	</div>
</div>