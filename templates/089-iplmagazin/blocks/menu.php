<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>

<?php if(!$detect->isMobile()) : ?>
	<nav class="navbar-wrapper">
      <div class="container">
        <div class="navbar">
          <div class="navbar-inner">
            <button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>           		              
			  <?php if ($this->countModules('menu')) : ?>
				<div class="nav-collapse collapse "  role="navigation">
					<jdoc:include type="modules" name="menu" style="custom" />
				</div>
				<?php endif; ?>			  
            <!--/.nav-collapse -->
          </div><!-- /.navbar-inner -->
        </div><!-- /.navbar -->

      </div> 
	  <!-- /.container -->
    </nav>
<?php else : ?>
			<div class="mm-btn">
		        <a href="#menu" class="btn-menu">
		            <div id="toggle" class="button_container">
		                <span class="top"></span>
		                <span class="middle"></span>
		                <span class="bottom"></span>
		            </div>
		        </a>
		    </div>
			<nav id="menu">
				<div class="menu-mobile"  role="navigation">
					<jdoc:include type="modules" name="menu-mobile" style="custom" />
				</div>
			</nav>
<?php endif;?>

