<?php
/**
 * @package     	Joomla.Site
 * @subpackage  	mod_menu override
 * @copyright   	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     	GNU General Public License version 2 or later; see LICENSE.txt
 * Modifications	Joomla CSS
 */

defined('_JEXEC') or die;
// Note. It is important to remove spaces between elements.

$detect = new Mobile_Detect;

?>
<?php // The menu class is deprecated. Use nav instead. ?>
<ul class="nav menu <?php echo $class_sfx;?>"<?php
	$tag = '';
	if ($params->get('tag_id') != null)
	{
		$tag = $params->get('tag_id').'';
		echo ' id="'.$tag.'"';
	}
?>>
<?php
foreach ($list as $i => &$item) :
	$class = 'item-'.$item->id;
	if ($item->id == $active_id)
	{
		$class .= ' current';
	}

	if (in_array($item->id, $path))
	{
		$class .= ' active';
	}
	elseif ($item->type == 'alias')
	{
		$aliasToId = $item->params->get('aliasoptions');
		if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
		{
			$class .= ' active';
		}
		elseif (in_array($aliasToId, $path))
		{
			$class .= ' alias-parent-active';
		}
	}

	if ($item->type == 'separator')
	{
		$class .= ' divider';
	}

	if ($item->deeper) {
		if ($item->level < 2) {
			$class .= ' dropdown deeper  mootools-noconflict';
			$first_start = true;
		}
		else {
			$class .= ' deeper-level';	/* CG: bei tiefer gehenden Menues diese KLasse entfernen' dropdown-submenu deeper'; und den den a tag des mmneu
										 * auf 50% width ca - da sonst der parent Link nicht mehr geklickt werden kann
										 */
		}
	}

	if ($item->parent)
	{
		$class .= ' parent';
	}

	if (!empty($class))
	{
		$class = ' class="'.trim($class) .'"';
	}

	echo '<li'.$class.'>';

	// Render the menu item.
	switch ($item->type) :
		case 'separator':
		case 'url':
		case 'component':
		case 'heading':
			require JModuleHelper::getLayoutPath('mod_menu', 'default_'.$item->type);
			break;

		default:
			require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
			break;
	endswitch;

	// The next item is deeper.
	if ($item->deeper){
		echo '<ul class="nav-child ">'; //CG hier die dropdown klassen entfernen!!
	}
	elseif ($item->deeper) {
		echo '<ul>';
	}
	// The next item is shallower.
	elseif ($item->shallower) {
		echo '</li>';
		echo str_repeat('</ul></li>', $item->level_diff);
	}
	// The next item is on the same level.
	else {
		echo '</li>';
	}
endforeach;
/* print ( $detect->isMobile() && !$detect->isTablet() ) ? 
								'<li>
									<a class="" href="/">Unternehmen</a>
										<ul>
											<li>
												<a href="/unternehmen/ueber-ipl-unternehmen.html">Die IPL Unternehmen</a>
											</li>
											<li>
												<a href="/unternehmen/ueber-ipl-gmbh.html">Über IPL GmbH</a>
											</li>
											<li>
												<a class=" menu__link" href="/unternehmen/kompetenzen.html">Kompetenzen</a>
											</li>
										</ul>
								<li>
									<a class="animsition-link" href="/aktuelles.html" >Aktuelles</a>
								</li>
								<li>
									<a class="animsition-link" href="/publikationen.html" >Publikationen</a>
								</li>
								<li>
									<a class="animsition-link" href="/stellenangebote.html" >Stellenangebote</a>
								</li>
								<li>
									<a class="animsition-link" href="/kontakt.html" >Kontakt</a>
								</li>								
								</ul>' : '</ul>'; 
*/
?>

