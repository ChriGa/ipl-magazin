<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$item_heading = $params->get('item_heading', 'h4');
/*
 * CG Override - Intro Image in custom newsflash Module
 */
	$images = json_decode($item->images);
	$intrImage = $images->image_intro; 
?>

<?php if ($params->get('item_title')) : ?>

	<<?php echo $item_heading; ?> class="newsflash-title">

	<?php if ($params->get('link_titles') && $item->link != '') : ?>
		<a href="<?php echo $item->link; ?>">
			<?php echo $item->title; ?>
		</a>
	<?php else : ?>
		<?php echo $item->title; ?>
	<?php endif; ?>
	</<?php echo $item_heading; ?>>

<?php endif; ?>
	<div class="<?php print ($intrImage) ? "span6" : "" ?> intrTextNews">

		<?php if (!$params->get('intro_only')) : ?>
			<?php echo $item->afterDisplayTitle; ?>
		<?php endif; ?>

		<?php echo $item->beforeDisplayContent; ?>

		<?php echo $item->introtext; ?>

		<?php if (isset($item->link) && $item->readmore != 0 && $params->get('readmore')) : ?>
			<?php echo '<a class="readmore" href="' . $item->link . '">' . $item->linkText . '</a>'; ?>
		<?php endif; ?>
	</div>
<?php 

if ($intrImage) : ?>
		<div class="span6 custonNewsImg">
			<img src="<?php print $intrImage; ?>" alt="<?php print $item_heading; ?>" />
		</div> 

	<div class="clr"></div>
<?php endif; ?>

	<?php /* <-- Override Ende --> */ ?>

