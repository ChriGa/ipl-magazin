<?php
/**
 * @author   Free-Joomla-Templates.com
 * @copyright   Copyright (C) 2014 Free-Joomla-Templates.com. All rights reserved.
 * @URL http://http://free-joomla-templates.com/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');
$ausgabeNo = $this->params->get('nummer');
$ausgabeMonat = $this->params->get('monat');

//Mag Banner Varibalen
$mbImage = $this->params->get('mbBild');
$imgDegree = $this->params->get('imgDegree');
$imgTop = $this->params->get('imgTop');
$mbHeadline = $this->params->get('mbHeadline');
$mbLink = $this->params->get('mbLink');



if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}


// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img class="logo-img" src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '-Logo" title="'.$sitename.' das Praxismagazin für Produktionsmanagement und Logistik " />';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle')) . '</span>';
}
else
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>