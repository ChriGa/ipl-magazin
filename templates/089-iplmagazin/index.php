<?php
/**
 * @author   	cg@089webdesign.de
 */
 

defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
//
?>
<!DOCTYPE html>
<html lang="De-de">
<head>
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
</head>

<body id="body" class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. $body_class; print ($detect->isMobile()) ? "mobile" : "desktop";
?>">
	<!-- Body -->
		<div class="<?php print (!$detect->isMobile()) ? "container" : "container-fluid"; ?> site_wrapper">
			<section id="section-header">
				<?php						
				// including header
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');		
				?>
			</section>
			<section id="section-menue">
				<?php
				// including header
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');
				?>
			</section>
			<section id="section-content">
				<?php
				// including slider
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/slider.php');

				// including top
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');							
						
				// including top
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');	
										
				// including content
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	
				
				// including bottom
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');
				?>
			</section>
			<section id="section-footer">	
				<?php
				// including content
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');							
				?>					
			</section>
			<?php if($frontPage && !$detect->isMobile()) print '<div id="tabSliderbkgr"></div>'; ?>
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />
<?php // scroll to top ?>
	<div class="scroll-top-wrapper ">
		<span class="scroll-top-inner">
			<span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
		</span>
	</div>
<script>	 
 	jQuery(function(){	 
		jQuery(document).on( 'scroll', function(){
	 
			if (jQuery(window).scrollTop() > 100) {
				jQuery('.scroll-top-wrapper').addClass('show');
			} else {
				jQuery('.scroll-top-wrapper').removeClass('show');
			}
		});	 
		jQuery('.scroll-top-wrapper').on('click', scrollToTop);
	});	 
	function scrollToTop() {
		verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
		element = jQuery('body');
		offset = element.offset();
		offsetTop = offset.top;
		jQuery('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
	}
</script>	
<?php // scroll to top ->> END ?>	
</body>
</html>
<?php if( !$detect->isMobile() || $detect->isTablet() ) : ?>
	<script type="text/javascript">
			jQuery(document).ready(function() {
			  jQuery(window).load(function() {
			    equalheight('.box-class');

			  });

			  jQuery(window).resize(function() {
			    equalheight('.box-class');

			  });
			});
	</script>
<?php endif; ?>
<?php if ($detect->isMobile()) : ?>
	<?php // mmenu ?>
		<script type="text/javascript">
			var $menu = jQuery('#menu');
			var $btnMenu = jQuery('.btn-menu');

			 jQuery(document).ready(function( $ ) {
			    jQuery("#menu").mmenu({
			       "extensions": [
			          "effect-menu-zoom",
			          "effect-panels-zoom",
			          "pagedim-black",
			          "pageshadow"
			       ],
			       "counters": true,
			       navbar: {
			             title: "IPL-GmbH Menü"       	
			       },
			      /* "navbars": [
			          {
			             "position": "bottom",
			             "content": [
			                "<a class='' href='/'>LINK ?</a>",
			                "<a class='' href='/'>LINK ?</a>",
			                "<a class='' href='/'>LINK ?</a>"
			             ]
			          }			       
			       ]*/
			    });
			 	$menu.find( ".mm-next" ).addClass("mm-fullsubopen");
			 });
		</script>
<?php endif; ?>
