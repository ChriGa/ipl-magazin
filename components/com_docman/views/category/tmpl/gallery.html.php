<?
/**
 * @package     DOCman
 * @copyright   Copyright (C) 2011 - 2014 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */
defined('KOOWA') or die; ?>

<?= helper('bootstrap.load'); ?>

<a class="koowa_header__link" href="<?= route('layout=gallery&slug=' . $category->slug) ?>">
    <? if ($category->image_path): ?>
        <div class="koowa_media__item__thumbnail koowa_media__item__folder">
            <img itemprop="thumbnail" src="<?= $category->image_path ?>"
                 alt="<?= escape($category->title) ?>">
        </div>
    <? else: ?>
        <div class="koowa_media__item__icon koowa_media__item__folder">
            <?= import('com://site/docman.document.icon.html', array(
                'icon'  => $category->icon == 'folder' ? 'image' : $category->icon,
                'class' => 'koowa_icon--48'
            )); ?>
        </div>
    <? endif ?>

    <div class="koowa_header koowa_media__item__label">
        <div class="koowa_header__item koowa_header__item--title_container">
            <div class="koowa_wrapped_content">
                <div class="whitespace_preserver">
                    <div class="overflow_container">
                        <?= escape($category->title) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</a>

