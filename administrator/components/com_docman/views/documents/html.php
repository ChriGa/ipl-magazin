<?php
/**
 * @package    DOCman
 * @copyright   Copyright (C) 2011 - 2014 Timble CVBA (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

class ComDocmanViewDocumentsHtml extends ComDocmanViewHtml
{
    protected function _fetchData(KViewContext $context)
    {
        $context->data->category_count = $this->getObject('com://admin/docman.model.categories')->count();

        parent::_fetchData($context);
    }
}
