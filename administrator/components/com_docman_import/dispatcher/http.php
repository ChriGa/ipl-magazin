<?php
/**
 * @package    DOCman
 * @copyright   Copyright (C) 2011 - 2014 Timble CVBA (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

class ComDocman_importDispatcherHttp extends ComKoowaDispatcherHttp
{
    public function __construct(KObjectConfig $config)
    {
        parent::__construct($config);

        $this->addCommandCallback('before.dispatch', 'checkManageRights');
    }

    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array(
            'controller' => 'import'
        ));

        parent::_initialize($config);
    }

    public function getRequest()
    {
        $request = parent::getRequest();

        $request->query->tmpl = 'koowa';
        $request->query->view = 'import';

        return $request;
    }

    public function checkManageRights(KDispatcherContextInterface $context)
    {
        if (!JFactory::getUser()->authorise('core.admin')) {
            JFactory::getApplication()->redirect('index.php', JText::_('JERROR_ALERTNOAUTHOR'), 'error');

            return false;
        }

        return true;
    }
}
