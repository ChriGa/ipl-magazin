<?php

/* -------------------------------------------
Component: com_MigrateMe
Author: Barnaby Dixon
Email: barnaby@php-web-design.com
Copywrite: Copywrite (C) 2013 Barnaby Dixon. All Rights Reserved.
License: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
---------------------------------------------*/

defined('_JEXEC') or die;
if (!JFactory::getUser()->authorise('core.manage', 'com_contact')) {
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}
defined('DS') or define('DS', '/');
require_once( JPATH_COMPONENT.DS.'controller.php' );
$controller = new MigrateMeController();
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();