<?php
/* -------------------------------------------
Component: com_MigrateMe
Author: Barnaby V. Dixon
Email: barnaby@php-web-design.com
Copywrite: Copywrite (C) 2013 Barnaby Dixon. All Rights Reserved.
License: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
---------------------------------------------*/
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');
jimport('joomla.application.component.helper');
if(!class_exists('JViewLegacy')) {
    class JViewLegacy extends JView {
        function __construct() {
            parent::__construct();
        }
    }
}
class MigrateMeViewdefault extends JViewLegacy {
	function display( $tpl = null ) {
	   
        require_once(JPATH_COMPONENT.'/resources/helpers/migrateme.class.php');
        $mm = new migrateMe;

        //GET MM COMPATIBILITY CHECKS
        $this->checks = $mm->compatibility;

        //CHECK FOR OLD MIGRATION ATTEMPT
        $this->resume = 0;
        if($mm->checkFileExists() !== FALSE) {
            $this->resume = 1;
        } else if ($mm->checkPreviousMigration() !== FALSE) {
            $this->resume = 2;
        }
        
		parent::display($tpl);
	}
}