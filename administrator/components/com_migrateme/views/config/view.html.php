<?php
/* -------------------------------------------
Component: com_MigrateMePLUS
Author: Barnaby V. Dixon
Email: barnaby@php-web-design.com
Copywrite: Copywrite (C) 2013 Barnaby Dixon. All Rights Reserved.
License: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
---------------------------------------------*/
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view');
jimport('joomla.application.component.helper');
if(!class_exists('JViewLegacy')) {
    class JViewLegacy extends JView {
        function __construct() {
            parent::__construct();
        }
    }
}
class MigrateMeViewconfig extends JViewLegacy {
	function display( $tpl = null ) {
	   
        //INITIALISE PARAMETERS
        $params = JComponentHelper::getParams('com_migrateme');
        
        //CHECK FOR TIMEOUT PARAMETER
        $this->speed = (int)$params->get('speed', 2);
        $this->overwrite = (int)$params->get('overwrite', 1);
        $this->aliases = (int)$params->get('aliases', 1);

		parent::display($tpl);
	}
}