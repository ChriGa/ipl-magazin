<?php

class ComExtmanViewExportHtml extends ComKoowaViewHtml
{
    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array(
            'auto_fetch' => false
        ));

        parent::_initialize($config);
    }

    protected function _fetchData(KViewContext $context)
    {
        parent::_fetchData($context);

        $data  = $this->getData();
        $exporters = KObjectConfig::unbox($data['exporters']);

        if (empty($exporters)) {
            $this->setLayout('error');
        }
        else
        {
            $labels = array();
            foreach ($exporters as $extension => $exporter)
            {
                $list = $exporter->getIterator();
                $configs = array();
                foreach ($list as $job) {
                    $configs[$job->name] = KObjectConfig::unbox($job);
                }

                $labels[$extension]    = $exporter->getLabel();
                $exporters[$extension] = $configs;
            }

            $context->data->token     = $this->getObject('user')->getSession()->getToken();
            $context->data->exporters = $exporters;
            $context->data->labels = $labels;
            $context->data->missing_dependencies = $this->getMissingDependencies();
        }
    }

    /**
     * Missing dependencies getter.
     *
     * @return array A list of missing dependencies.
     */
    public function getMissingDependencies()
    {
        $requirements = array(
            'zip' => array(
                class_exists('ZipArchive'),
                'ZipArchive class is needed for the export process.'
            ),
            'tmp' => array(
                is_writable(JPATH_ROOT.'/tmp'),
                'Please make sure tmp directory in your site root is writable'
            )
        );

        $return = array();

        foreach ($requirements as $key => $value)
        {
            if ($value[0] === false) {
                $return[$key] = $value[1];
            }
        }

        return $return;
    }
}
