<?php
/**
 * @package     DOCman Exporter
 * @copyright   Copyright (C) 2011 - 2014 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * DOCman Exporter Class.
 */
class ComExtmanMigratorLogmanExport extends ComExtmanMigratorExportAbstract
{
    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array(
                'label'     => 'LOGman',
                'extension' => 'logman',
                'jobs'  => array(
                    'export_logs'          => array(
                        'label'      => 'Exporting logs',
                        'table'      => 'logman_activities'
                    ),
                    'export_assets'             => array(
                        'label'      => 'Exporting assets',
                        'table'      => 'assets',
                        'callback'   => function($query) {
                            $query->where("name LIKE :assets")->bind(array(
                                'assets' => 'com_logman%'
                            ));
                        }
                    ),
                    'export_settings'           => array(
                        'label'      => 'Exporting settings',
                        'table'      => 'extensions',
                        'callback'  => function($query) {
                            $query->where("element = :element")->bind(array(
                                'element' => 'com_logman'
                            ));
                        }
                    )
                )
            )
        );

        parent::_initialize($config);
    }
}