<?php
/**
 * @package     DOCman Exporter
 * @copyright   Copyright (C) 2011 - 2014 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * DOCman Importer Class.
 */
class ComExtmanMigratorLogmanImport extends ComExtmanMigratorImportAbstract
{
    protected function _initialize(KObjectConfig $config)
    {
        $source = substr($config->source_version, 0, 3);

        $config->append(array(
            'label'     => 'LOGman',
            'extension' => 'logman',
            'jobs'      => array()
        ));

        $config->append(array(
            'jobs'      => array(
                'insert_activities' => array(
                    'action'    => 'insert',
                    'chunkable' => true,
                    'label'     => 'Inserting activities',
                    'source'    => 'logman_activities',
                    'table'     => 'logman_activities_mig',
                    'create_from'    => 'logman_activities'
                ),
                'insert_assets' => array(
                    'action'      => 'insert',
                    'chunkable'   => true,
                    'label'       => 'Inserting assets',
                    'source'      => 'assets',
                    'table'       => 'logman_assets_mig',
                    'create_from' => 'assets'
                ),
                'move_activities' => array(
                    'action' => 'move',
                    'label'  => 'Moving activities',
                    'source' => 'logman_activities_mig',
                    'target' => 'logman_activities'
                ),
                'import_assets' => array(
                    'action'    => 'import_assets',
                    'label'     => 'Importing assets',
                    'source'    => 'logman_assets_mig',
                    'target'    => 'assets',
                    'extension' => 'com_logman'
                ),
                'import_settings' => array(
                    'action' => 'import_settings',
                    'label'  => 'Importing settings'
                )
            )
        ));

        parent::_initialize($config);
    }
}