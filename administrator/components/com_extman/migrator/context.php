<?php
/**
 * Nooku Framework - http://nooku.org/framework
 *
 * @copyright   Copyright (C) 2015 Johan Janssens and Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://github.com/nooku/nooku-scheduler for the canonical source repository
 */

/**
 * Job context
 *
 * @author Ercan Ozkaya <https://github.com/ercanozkaya>
 * @package Koowa\Component\Scheduler
 */
class ComExtmanMigratorContext extends KControllerContext
{
    /**
     * @var KObjectConfig
     */
    protected $_job;

    /**
     * @var string
     */
    protected $_error;

    /**
     * @return string
     */
    public function getError()
    {
        return $this->_error;
    }

    /**
     * @param string $error
     * @return $this
     */
    public function setError($error)
    {
        $this->_error = $error;

        return $this;
    }

    /**
     * @return KObjectConfig
     */
    public function getJob()
    {
        return $this->_job;
    }

    /**
     * @param KObjectConfig $job
     * @return $this
     */
    public function setJob(KObjectConfig $job)
    {
        $this->_job = $job;

        return $this;
    }
}